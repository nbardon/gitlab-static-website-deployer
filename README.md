# Gitlab Static Website Deployer
Download GitLab release and extract it to a public www folder.


## Install dependencies and run

```bash
bun install
bun run .
```

## Environment variables

 - `HTTP_PORT` : Set the webserver port (default : 2102).
 - `TLS_CERT` : Path to TLS fullchain certificate (default : null).
 - `TLS_PRIVKEY` : Path to TLS private key (default : null).
 - `PROJECTS_DB` : Path to projects database (default : projects.json).


> If `TLS_CERT` and `TLS_PRIVKEY` are defined, TLS is enabled on webserver.
