const API_BASE = "https://gitlab.com/api/v4";
const fs = require("fs/promises");
const zip = require("adm-zip");
const path = require("path");

let cok   = (msg) => console.log(`[\x1b[32m OK  \x1b[0m] ${msg}`);
let cinfo = (msg) => console.log(`[\x1b[34mINFO \x1b[0m] ${msg}`);
let cerr  = (msg) => console.log(`[\x1b[31mERROR\x1b[0m] ${msg}`);

let projects = {};

async function downloadLastRelease(projectID) {
	cinfo(`Project ${projects[projectID].name} : Fetching last assets...`);
	let query = await fetch(`${API_BASE}/projects/${projectID}/releases?page=1&per_page=1`);
	let last_release = await query.json();
	if (last_release.length < 0)
		throw "No release available";

	last_release = last_release[0];
	if (last_release.assets.links.length < 0)
		throw "No asset available";

	cinfo(`Project ${projects[projectID].name} : Version ${last_release.name} available (${last_release.released_at}). Downloading assets...`);
	query = await fetch(last_release.assets.links[0].url);
	let zip_buffer = Buffer.from(await query.arrayBuffer());

	cinfo(`Project ${projects[projectID].name} : Version ${last_release.name} downloaded. Unzipping...`);
	return zip(zip_buffer);
}

async function backupOld(projectID) {
	let d = new Date();
	let dateStr = `${d.getFullYear()}.${d.getMonth()+1}.${d.getDate()} - ${d.getHours()}h${d.getMinutes()}m${d.getSeconds()}.${d.getMilliseconds()}`;
	let dest = path.join(projects[projectID].path, "_old", dateStr);

	cinfo(`Project ${projects[projectID].name} : Creating backup directory (${dest})...`);
	fs.mkdir(dest, { recursive: true });

	cinfo(`Project ${projects[projectID].name} : Moving files to backup directory...`);
	let files = await fs.readdir(projects[projectID].path);
	for (let f of files) {
		if (f == '_old')
			continue;

		await fs.rename(
			path.join(projects[projectID].path, f),
			path.join(dest, f)
		);
	}
	cok(`Project ${projects[projectID].name} : Backup done.`);
}

async function deployRelease(projectID, assets) {
	let dest = await fs.stat(projects[projectID].path);
	if (dest.isDirectory())
		await backupOld(projectID);
	else
		await fs.mkdir(projects[projectID].path, { recursive: true });

	cinfo(`Project ${projects[projectID].name} : Extracting assets to project directory...`);

	assets.getEntries().forEach( entry => {
		if (entry.isDirectory || !entry.entryName.startsWith("dist/"))
			return;

		let entry_dest = path.join(
			projects[projectID].path,
			entry.entryName.substring(5, entry.entryName.lastIndexOf('/'))
		);
		assets.extractEntryTo(entry, entry_dest, false, false);
	});
	cok(`Project ${projects[projectID].name} : Updated successfully`);
}


async function update (projectID) {
	try {
		let asset = await downloadLastRelease(projectID);
		deployRelease(projectID, asset);
	} catch (e) {
		cerr(`Project ${projects[project_id].name} : ${e}`);
	}
}


/////////////////////////////////////////////////////////

async function main() {
	try {
		let project_db_paths = process.env.PROJECTS_DB || "projects.json";
		cinfo(`Reading project database : ${project_db_paths}...`);
		projects = await Bun.file(project_db_paths).json();

		let tlsKey = null, tlsCert = null;
		if (process.env.hasOwnProperty('TLS_PRIVKEY') &&
		    process.env.hasOwnProperty('TLS_CERT')) {
			cinfo("HTTP : Enabling TLS");
			cinfo(`TLS : Using certificate ${process.env.TLS_CERT}`);
			cinfo(`TLS : Using key file ${process.env.TLS_PRIVKEY}`);

			tlsKey = Bun.file(process.env.TLS_PRIVKEY);
			tlsCert = Bun.file(process.env.TLS_CERT);
		}

		const server = Bun.serve({
			development: false,
			port: process.env.HTTP_PORT || 2102,
			key: tlsKey,
			cert: tlsCert,
			async fetch(req) {
				let clientIP = server.requestIP(req);
				try { clientIP = `${clientIP.family =='IPv6' ? '[' : ''}${clientIP.address}${clientIP.family =='IPv6' ? ']' : ''}:${clientIP.port}`; } catch {}

				let matches = req.url.match(/^https?:\/\/[^\/]+\/(\d+)\/?$/);
				if (req.method != "POST" || req.headers.get('Content-Type') != "application/json" ||
				    req.headers.get('x-gitlab-event') != "Release Hook" || matches == null || matches.length < 2)
					return new Response("Bad Request.", { status: 400 });

			//	console.dir(await req.json());


				let projectID = matches[1];
				if (!projects.hasOwnProperty(projectID)) {
					cerr(`Client ${clientIP} request an update for an unregistered project (${matches[1]})`);
					return new Response("Project not found", { status: 404 });
				}

				cinfo(`Client ${clientIP} request an update for ${projects[projectID].name}`);
				update(projectID);

				return new Response("Updating project...");
			}
		});

		cinfo(`Listening on http${tlsKey != null ? 's' : ''}://localhost:${server.port} ...`);
	} catch (e) {
		cerr(e);
	}
}

main();
